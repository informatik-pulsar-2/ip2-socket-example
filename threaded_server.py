import socket
import threading

###
# FROM https://stackoverflow.com/a/23828265/5712160
###

class ThreadedServer(object):
    def __init__(self, host="0.0.0.0", port=8123, size=1024):
        self.host = host
        self.port = port
        self.size = size
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        
        self.clients = []

    def listen(self):
        self.sock.listen(8)
        while True:
            client, address = self.sock.accept()
#            client.settimeout(60)
            threading.Thread(target = self.listenToClient, args = (client, address)).start()

    def listenToClient(self, client, address):
        print("Connection from " + str(address))
        self.clients.append(client)
        while True:
            try:
                data = client.recv(self.size)
                if data:
                    msg = data.decode("utf-8")
                    print(str(address) + ": " + msg)
                    
#                    client.send(data)
                    for cclient in self.clients:
                        if cclient:
                            cclient.send((str(address) + ": " + msg).encode("utf-8"))
                else:
                    print(str(address) + " disconnected")
                    raise Exception("client disconnected")
            except Exception as e:
                print("Closing connection to " + str(address), e)
                self.clients.remove(client)
                client.close()
                return False

if __name__ == "__main__":
#    while True:
#        port_num = input("Port? ")
#        try:
#            port_num = int(port_num)
#            break
#        except ValueError:
#            pass
    
#    host = input("host: ")
#    port = int(input("port: "))

    ThreadedServer().listen()