# socket-example

## How to use

Start server:

```shell
python3 server.py
```

Start client (may be started again indefinitely):

```shell
python3 client.py
```

## Sources:

- https://duckduckgo.com/?q=python3+tcp+socket&t=ffab&ia=web
- https://www.techbeamers.com/python-tutorial-write-tcp-server/
- https://docs.python.org/3/library/socketserver.html
- https://www.tutorialspoint.com/python3/python_networking.htm
- https://docs.python.org/3/howto/sockets.html
- https://docs.python.org/3/library/socket.html
- https://wiki.python.org/moin/TcpCommunication
- https://www.geeksforgeeks.org/simple-chat-room-using-python/

