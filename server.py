# -*- coding: utf-8 -*-

import socket
#import atexit
#
#def exitFunc():
#    print("exiting..")
#    s.shutdown(socket.SHUT_RDWR)
#    s.close()
#
#atexit.register(exitFunc)

# create a socket object
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # make addr binding directly usable again after exit
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # get local machine name
    #host = socket.gethostname()
    host = "0.0.0.0"

    port = 8123

    # bind to the port
    s.bind((host, port))

    # queue up to 5 requests
    s.listen(5)

    while True:
        # establish a connection
        cs,addr = s.accept()

        with cs:
            print("Connection from %s" % str(addr))

            while True:
                # Receive no more than 1024 bytes
                msg = cs.recv(1024)

                if len(msg) > 0:
                    print(msg.decode('utf-8'))
                else:
                    break
