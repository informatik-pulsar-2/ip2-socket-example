# -*- coding: utf-8 -*-

import socket
import threading

class ThreadedClient(object):
    def __init__(self, host="localhost", port=8123, size=1024):
        self.host = host
        self.port = port
        self.size = size
        
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def run(self):
        self.sock.connect((self.host, self.port))
        threading.Thread(target = self.send).start()
        threading.Thread(target = self.receive).start()

    def send(self):
        while True:
    	    msg = input("msg: ")
    	    self.sock.send(msg.encode("utf-8"))

    def receive(self):
        while True:
            try:
                data = self.sock.recv(self.size)
                if data:
                    msg = data.decode("utf-8")
                    print(">> " + msg)
                else:
                    print("disconnected")
                    raise Exception("server disconnected")
            except:
                self.sock.close()
                return False

if __name__ == "__main__":
#    host = input("host: ")
#    port = int(input("port: "))

    ThreadedClient().run()
