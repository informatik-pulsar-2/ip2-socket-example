# -*- coding: utf-8 -*-

import socket

# create a socket object
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # get local machine name
    #host = socket.gethostname()
    host = input("host: ")

    port = int(input("port: "))

    # connection to hostname on the port.
    s.connect((host, port))

    while True:
	    msg = input("msg: ")
	    s.send(msg.encode('utf-8'))
